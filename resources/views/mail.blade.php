<!DOCTYPE html>
<html lang="">
<head>
    <title>New Signup Invitation Email</title>
</head>
<body>

<h1>Invitation for New Signup</h1>
<p>Please follow the link to complete your signup process <a href="{{url('user/register')}}">HERE</a></p>

</body>
</html>
