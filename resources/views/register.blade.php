<!DOCTYPE html>
<!-- Created By CodingLab - www.codinglabweb.com -->
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap');
        *{
            margin: 0 ;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Poppins',sans-serif;
        }
        body{
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 10px;
            background: linear-gradient(135deg, #71b7e6, #9b59b6);
        }
        .container{
            max-width: 700px;
            width: 100%;
            background-color: #fff;
            padding: 25px 30px;
            border-radius: 5px;
            box-shadow: 0 5px 10px rgba(0,0,0,0.15);
        }
        .container .title{
            font-size: 25px;
            font-weight: 500;
            position: relative;
        }
        .container .title::before{
            content: "";
            position: absolute;
            left: 0;
            bottom: 0;
            height: 3px;
            width: 30px;
            border-radius: 5px;
            background: linear-gradient(135deg, #71b7e6, #9b59b6);
        }
        .content form .user-details{
            display: flex !important;
            flex-wrap: wrap !important;
            justify-content: space-between !important;
            margin: 20px 0 12px 0 !important;
        }
        form .user-details .input-box{
            margin-bottom: 15px !important;
            width: calc(100% / 2 - 20px) !important;
        }
        form .input-box span.details{
            display: block !important;
            font-weight: 500 !important;
            margin-bottom: 5px !important;
        }
        .user-details .input-box input{
            height: 45px !important;
            width: 100% !important;
            outline: none !important;
            font-size: 16px !important;
            border-radius: 5px !important;
            padding-left: 15px !important;
            border: 1px solid #ccc !important;
            border-bottom-width: 2px !important;
            transition: all 0.3s ease !important;
        }
        .user-details .input-box input:focus,
        .user-details .input-box input:valid{
            border-color: #9b59b6 !important;
        }
        form .gender-details .gender-title{
            font-size: 20px;
            font-weight: 500;
        }
        form .category{
            display: flex;
            width: 80%;
            margin: 14px 0 ;
            justify-content: space-between;
        }
        form .category label{
            display: flex;
            align-items: center;
            cursor: pointer;
        }
        form .category label .dot{
            height: 18px;
            width: 18px;
            border-radius: 50%;
            margin-right: 10px;
            background: #d9d9d9;
            border: 5px solid transparent;
            transition: all 0.3s ease;
        }
        #dot-1:checked ~ .category label .one,
        #dot-2:checked ~ .category label .two,
        #dot-3:checked ~ .category label .three{
            background: #9b59b6;
            border-color: #d9d9d9;
        }
        form input[type="radio"]{
            display: none;
        }
        form .button{
            height: 45px;
            margin: 35px 0
        }
        form .button input{
            height: 100%;
            width: 100%;
            border-radius: 5px;
            border: none ;
            color: #fff ;
            font-size: 18px;
            font-weight: 500;
            letter-spacing: 1px;
            cursor: pointer;
            transition: all 0.3s ease;
            background: linear-gradient(135deg, #71b7e6, #9b59b6);
        }
        form .button input:hover{
            /* transform: scale(0.99); */
            background: linear-gradient(-135deg, #71b7e6, #9b59b6) ;
        }
        @media(max-width: 584px){
            .container{
                max-width: 100%;
            }
            form .user-details .input-box{
                margin-bottom: 15px;
                width: 100%;
            }
            form .category{
                width: 100% !important;
            }
            .content form .user-details{
                max-height: 300px !important;
                overflow-y: scroll !important;
            }
            .user-details::-webkit-scrollbar{
                width: 5px !important;
            }
        }
        @media(max-width: 459px){
            .container .content .category{
                flex-direction: column !important;
            }
        }

    </style>
</head>
<body>
<div class="container">
    <div class="title">Registration</div>
    <div class="content">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-block">
                <strong>{{ $error }}</strong>
            </div>
        @endforeach

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <strong style="color: green;">{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <form action="{{route('save_user')}}" method="POST">
            @csrf
            <div class="user-details">
                <div class="input-box">
                    <span class="details">Full Name</span>
                    <label>
                        <input type="text" name="name" placeholder="Enter your name" required>
                    </label>
                </div>
                <div class="input-box">
                    <span class="details">Email</span>
                    <label>
                        <input type="text" name="email" placeholder="Enter your email" required>
                    </label>
                </div>
                <div class="input-box">
                    <span class="details">Password</span>
                    <label>
                        <input type="password" name="password" placeholder="Enter your password" required>
                    </label>
                </div>
                <div class="input-box">
                    <span class="details">Confirm Password</span>
                    <label>
                        <input type="password" name="password_confirm" placeholder="Confirm your password" required>
                    </label>
                </div>
            </div>
            <div class="button">
                <input type="submit" value="Register">
            </div>
        </form>
    </div>
</div>

</body>
</html>
