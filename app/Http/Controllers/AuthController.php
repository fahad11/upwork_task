<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){

        if (!Auth::attempt(["email" => $request->email, "password" => $request->password, "is_admin" => 1])) {
            return response()->json([
                "success" => false,
                "message" => "Invalid login details"
            ], 401);
        }

        $user = User::where('email' , $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'success' => true,
            'message'   =>  'Admin LoggedIn Successfully',
            'token' => $token
        ]);
    }
    public function get_userLogin(Request $request){

        if (!Auth::attempt(["email" => $request->email, "password" => $request->password, "is_admin" => 0])) {
            return response()->json([
                "success" => false,
                "message" => "Invalid login details"
            ], 401);
        }
        $user = User::where('email' , $request['email'])->firstOrFail();
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json([
            'success' => true,
            'message'   =>  'User LoggedIn Successfully',
            'token' => $token
        ]);
    }
}
