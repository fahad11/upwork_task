<?php

namespace App\Http\Controllers;

use App\Mail\NotifyMail;
use App\Notifications\SendVerificationCodeNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Models\User;
use App\Notifications\MailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function sendSignUpInvitation(Request $request)
    {

        Mail::to($request->email)->send(new NotifyMail());

        if (Mail::failures()) {
            return response()->json(["success" => true, "message" => "Sorry! Please try again later"]);
        }
        return response()->json(["success" => true, "message" => "Invitation Link has been sent!!"]);
    }
    public function register(){
        return view('register');
    }
    public function save(Request $request){

        $rules = array(
            'name'             => 'required',
            'email'            => 'required|email|unique:users',
            'password'         => 'required',
            'password_confirm' => 'required|same:password'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
//            $messages = $validator->messages();
            return Redirect::to('user/register')
                ->withErrors($validator);

        } else {

            $user = new User();
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            $isSaved = $user->save();
            if($isSaved){
                Notification::send($user,new SendVerificationCodeNotification($user));
                return Redirect::to('user/verify/'.$user->id);
            }
            return Redirect::to('user/register');
        }
    }
    public function userVerification($userId){
        return view('verify',compact('userId'));
    }
    public function verifyUserByRegistrationCode(Request $request){

        $code = $request->code;
        $user = User::where("verification_code",$code)->first();

        if(User::where("verification_code",$code)->exists()){
            $isUpdated = User::where("id",$user->id)->update(["is_registration_completed" => 1,"verification_code" => null]);
            if($isUpdated){
                return Redirect::to("user/register")->withErrors(["success" => "Registration Completed Successfully"]);
            }
            return Redirect::back()->withErrors(["error" => "Unable to update record"]);
        }
        return Redirect::back()->withErrors(["error" => "code doesn't match"]);
    }
    public function updateUserProfile(Request $request){
        $authUser = Auth::user()->getAuthIdentifier();
        $isDAtaUpdated = User::where("id",$authUser)->update($request->all());

        if($isDAtaUpdated){
            return response()->json([
                'success' => true,
                'message'   =>  'User profile updated Successfully'
            ]);
        }
        return response()->json([
            'success' => false,
            'message'   =>  'User profile unable to updated'
        ]);
    }

}
