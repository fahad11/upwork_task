<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController as Auth;
use App\Http\Controllers\UsersController as Users;

Route::group(['prefix' => 'admin'], function(){
    Route::post('/login',[Auth::class,'login']);
    Route::group(['middleware' => 'auth:sanctum'],function(){
        Route::post('/signup-invitation',[Users::class,'sendSignUpInvitation']);
    });
});

Route::group(['prefix' => 'user'], function(){
    Route::post('/login',[Auth::class,'get_userLogin']);
    Route::group(['middleware' => 'auth:sanctum'],function(){
        Route::post('update-user-profile',[Users::class,'updateUserProfile']);
    });
});
