<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController as Users;

Route::group(['prefix' => 'user'], function(){
    Route::get('/register',[Users::class,'register'])->name('register');
    Route::post('/save_user',[Users::class,'save'])->name('save_user');
    Route::get('/verify/{userId}',[Users::class,'userVerification']);
    Route::post('/is_user_registration_completed',[Users::class,'verifyUserByRegistrationCode'])->name('is_user_registration_completed');
});
